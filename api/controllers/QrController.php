<?php

namespace api\controllers;

use api\models\QrForm;
use api\models\QrFormUpdate;
use common\models\QrCodes;
use yii\db\Exception;
use yii\filters\ContentNegotiator;
use yii\rest\Controller;
use yii\web\HttpException;
use yii\web\Response;

class QrController extends Controller
{
    public function init()
    {
        parent::init();
        \Yii::$app->user->enableSession = false;
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        parent::behaviors();

        return [
            'contentNegotiator' => [
                'class' => ContentNegotiator::className(),
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }

    protected function verbs()
    {
        return [
            'view'      => ['GET'],
            'create'    => ['POST'],
            'update'    => ['PUT'],
        ];
    }

    public function actionCreate()
    {
        $model = new QrForm();

        try {
            if ($model->load(\Yii::$app->request->post(), '') && $qr = $model->create()) {
                return $qr;

            } else {
                $errors = $model->firstErrors;

                \Yii::$app->response->setStatusCode(422);
                throw new Exception(reset($errors));
            }
        }catch (\Exception $e){
            throw new HttpException(403, $e->getMessage());
        }
    }

    public function actionView($hash)
    {
        return QrCodes::getCode($hash);
    }

    public function actionUpdate()
    {
        $model = new QrFormUpdate();

        try {
            if ($model->load(\Yii::$app->request->post(), '') && $qr = $model->update()) {
                return $qr;

            } else {
                $errors = $model->firstErrors;

                \Yii::$app->response->setStatusCode(422);
                throw new Exception(reset($errors));
            }
        }catch (\Exception $e){
            throw new HttpException(403, $e->getMessage());
        }
    }
}