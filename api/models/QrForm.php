<?php

namespace api\models;

use common\models\QrCodes;
use common\models\QrCodesTypes;
use yii\base\Model;

class QrForm extends Model
{
    public $url;
    public $status;
    public $type;

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'url'           => 'url',
            'status'        => 'Статус',
            'type'          => 'Тип',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['url', 'status', 'type'], 'required'],

            [['status'], 'integer'],
            ['status', 'in', 'range' => [0, 1]],

            ['type', 'in', 'range' => ['user', 'merchant']],

            [['url', 'type'], 'string', 'max' => 255],
        ];
    }

    /**
     * Create new qr
     * @return mixed
     */
    public function create()
    {
        if ($this->validate()) {
            $qr             = new QrCodes();
            $qr->url        = $this->url;
            $qr->status     = $this->status;
            $qr->type_id    = $this->findType($this->type);
            $qr->hash       = \Yii::$app->security->generateRandomString() . time();

            if($qr->save()){
                return \Yii::$app->request->hostInfo . '/api/qr/' . $qr->hash;
            }
        }

        return false;
    }


    private function findType($type)
    {
        $model  = QrCodesTypes::findOne(['name' => $type]);
        if($model !== null){
            return $model->id;
        }

        return  null;
    }
}