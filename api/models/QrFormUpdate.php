<?php

namespace api\models;

use common\models\QrCodes;
use common\models\QrCodesTypes;
use yii\base\Model;

class QrFormUpdate extends Model
{
    public $hash;
    public $status;

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'status'        => 'Статус',
            'hash'          => 'hash',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['hash', 'status'], 'required'],

            [['status'], 'integer'],
            ['status', 'in', 'range' => [0, 1]],

            [['hash'], 'string', 'max' => 255],
        ];
    }

    /**
     * update qr
     * @return mixed
     */
    public function update()
    {
        $qr             = QrCodes::findOne(['hash' => $this->hash]);

        if ($this->validate() && $qr !== null) {
            $qr->status     = $this->status;

            if($qr->save()){
                return  $qr;
            }
        }

        return false;
    }
}