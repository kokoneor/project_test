<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%qr_codes}}`.
 */
class m200730_113211_create_qr_codes_table extends Migration
{
    public $table               = 'qr_codes';
    public $typeTable           = 'qr_codes_types';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable("{{{$this->table}}}", [
            'id'            => $this->primaryKey(),
            'type_id'       => $this->integer()->null(),
            'hash'          => $this->string(255)->null(),
            'status'        => $this->integer()->null()->defaultValue(1),
            'url'           => $this->string(255)->null(),
            'created_at'    => $this->timestamp()->null(),
        ], $tableOptions);

        $onUpdateConstraint = 'RESTRICT';
        if ($this->db->driverName === 'sqlsrv') {
            $onUpdateConstraint = 'NO ACTION';
        }

        $this->addForeignKey("fk_{$this->table}_{$this->typeTable}",
            "{{{$this->table}}}", 'type_id',
            "{{{$this->typeTable}}}", 'id',
            'CASCADE', $onUpdateConstraint);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey("fk_{$this->table}_{$this->typeTable}",
            "{{{$this->typeTable}}}");
        $this->dropTable("{{{$this->table}}}");
    }
}
