<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%qr_codes_types}}`.
 */
class m200730_112849_create_qr_codes_types_table extends Migration
{
    public $table               = 'qr_codes_types';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable("{{{$this->table}}}", [
            'id'            => $this->primaryKey(),
            'name'          => $this->string(255)->notNull(),
            'created_at'    => $this->timestamp()->null(),
        ], $tableOptions);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable("{{{$this->table}}}");
    }
}
