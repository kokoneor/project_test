<?php

namespace backend\controllers;

use yii\db\ActiveRecord;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

class BackendController extends Controller
{
    /**
     * Default active record model class
     * @var ActiveRecord
     */
    protected $model;
    /**
     * Default active record search model class
     * @var ActiveRecord
     */
    protected $searchModel;
    /**
     * Permissions array
     * @var array
     */
    protected $permissions = [
        'create' => null,
        'view'   => null,
        'update' => null,
        'index'  => null,
        'delete' => null,
    ];

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'verbs' => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'index'   => ['get'],
                    'view'    => ['get'],
                    'create'  => ['get', 'post'],
                    'update'  => ['get', 'put', 'post'],
                    'delete'  => ['post'],
                ],
            ],
        ]);
    }

    public function init()
    {
        parent::init(); // TODO: Change the autogenerated stub
    }

    /**
     * Lists all models.
     * @return mixed
     * @throws ForbiddenHttpException
     */
    public function actionIndex()
    {
        if (!\Yii::$app->user->can($this->permissions['index'])) {
            throw new ForbiddenHttpException('У вас недостаточно прав на просмотр');
        }

        /* @var $searchModel ActiveRecord */
        $searchModel    = new $this->searchModel;
        $dataProvider   = $searchModel->search(\Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel'   => $searchModel,
            'dataProvider'  => $dataProvider,
        ]);
    }

    /**
     * Displays a single model.
     * @param $id
     * @return string
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        if (!\Yii::$app->user->can($this->permissions['view'])) {
            throw new ForbiddenHttpException('У вас недостаточно прав на просмотр');
        }

        $model  = $this->findModel($id);

        return $this->render('view', [
                'model'     => $model
        ]);
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     * @throws ForbiddenHttpException
     */
    public function actionCreate()
    {
        if (false === \Yii::$app->user->can($this->permissions['create'])) {
            throw new ForbiddenHttpException('У вас недостаточно прав на создание');
        }

        /* @var $model ActiveRecord */
        $model  = new $this->model;

        if ($model->load(\Yii::$app->request->post()) && $model->save()) {
            \Yii::$app->session->setFlash('success', 'Объект успешно создан и сохранен!');

            return $this->redirect(['view', 'id' => $model->primaryKey]);
        } else {
            return $this->render('create', ['model' => $model]);
        }
    }

    /**
     * Updates an existing model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return string|\yii\web\Response
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        if (!\Yii::$app->user->can($this->permissions['update'])) {
            throw new ForbiddenHttpException('У вас недостаточно прав на изменение');
        }

        /* @var $model ActiveRecord */
        $model  = $this->findModel($id);

        if ($model->load(\Yii::$app->request->post()) && $model->save()) {
            \Yii::$app->session->setFlash('info', 'Изменения приняты!');

            return $this->redirect(['view', 'id' => $model->primaryKey]);
        } else {
            return $this->render('update', ['model' => $model]);
        }
    }

    /**
     * Deletes an existing model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param $id
     * @return \yii\web\Response
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     * @throws
     */
    public function actionDelete($id)
    {
        if (!\Yii::$app->user->can($this->permissions['delete'])) {
            throw new ForbiddenHttpException('У вас недостаточно прав на удаление');
        }

        /* @var $model ActiveRecord */
        $model  = $this->findModel($id);

        if ($model->delete()) {
            \Yii::$app->session->setFlash('success', 'Объект удален!');
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the Content model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return ActiveRecord the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        /* @var $activeRecord ActiveRecord */
        $activeRecord = new $this->model;

        if (null === $model = $activeRecord::findOne($id)) {
            throw new NotFoundHttpException('Указанная страница не найдена.');
        }

        return $model;
    }
}