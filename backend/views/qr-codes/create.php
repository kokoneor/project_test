<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\QrCodes */

$this->title = 'Create Qr Codes';
$this->params['breadcrumbs'][] = ['label' => 'Qr Codes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="qr-codes-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
