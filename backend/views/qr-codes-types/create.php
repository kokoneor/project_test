<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\QrCodesTypes */

$this->title = 'Create Qr Codes Types';
$this->params['breadcrumbs'][] = ['label' => 'Qr Codes Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="qr-codes-types-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
